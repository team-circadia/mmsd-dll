This is a fork of Michael Pyne's fork of Game Music Emu.
https://bitbucket.org/mpyne/game-music-emu
All code not relating to NSF support has been removed, so the LGPL is in force.

CMake and a C compiler such as Visual Studio are required, otherwise just run the build.bat to build.
