#ifdef __cplusplus
#define GMREAL extern "C" __declspec(dllexport) double __cdecl
#define GMSTRING extern "C" __declspec(dllexport) const char* __cdecl
#else
#define GMREAL __declspec(dllexport) double __cdecl
#define GMSTRING __declspec(dllexport) const char* __cdecl
#endif
#define _WIN32_WINNT 0x0602
#include <windows.h>