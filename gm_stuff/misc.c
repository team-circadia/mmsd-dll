#include "common.h"

#pragma comment(lib, "user32.lib")

GMREAL get_refresh_rate() {
	DEVMODEA devMode = {0};
	devMode.dmSize = sizeof(devMode);
	if (!EnumDisplaySettingsA(NULL, ENUM_CURRENT_SETTINGS, &devMode))
		return 0;
	return devMode.dmDisplayFrequency;
}