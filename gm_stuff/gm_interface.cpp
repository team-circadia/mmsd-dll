#include "common.h"
#include "../gme/gme.h"
#include "../gme/Music_Emu.h"
#include <string.h>
#include <math.h>
#include <xaudio2.h>

#pragma comment(lib, "user32.lib")

#include <stdio.h>

gme_err_t last_err = NULL;
Music_Emu* emu = NULL;
int paused = false;
volatile int playing = false;
double volume = 1.0;
int sample_rate = 44100;


class MusicCallback;

IXAudio2* pXAudio2 = NULL;
IXAudio2MasteringVoice* pMasteringVoice = NULL;
IXAudio2SourceVoice* pMusicVoice = NULL;
MusicCallback* pMusicCallback = NULL;
IXAudio2SubmixVoice* pSFXSubmixVoice = NULL;
#define SFX_CHANNEL_COUNT 2
IXAudio2SourceVoice* pSfxVoices[SFX_CHANNEL_COUNT] = {0};
#define SFX_LOOP_CHANNEL_COUNT 2
IXAudio2SourceVoice* pSfxVoicesLoop[SFX_LOOP_CHANNEL_COUNT] = {0};
int last_loop_channel;
unsigned int sfx_counts[5];
unsigned int* sfx_pointers = NULL;
BYTE* sfx_data = NULL;
int last_sfx_played[SFX_CHANNEL_COUNT];

GMSTRING GME_GetError() {
	return last_err;
}

GMREAL GME_LoadFile(const void *data, double size) {
	if (pMusicVoice != NULL) {
		while (playing); // don't mess with anything if stuff's going on
		pMusicVoice->Stop();
	}
	if (emu != NULL)
		gme_delete(emu);
	last_err = gme_open_data(data, (long)size, &emu, sample_rate);
	if (last_err != NULL)
		return 1;
	return 0;
}

GMREAL GME_StartTrack(double id) {
	if (emu == NULL) {
		last_err = "No file has been loaded";
		return 1;
	}
	if (pMusicVoice != NULL) {
		while (playing); // don't mess with anything if stuff's going on
		pMusicVoice->Stop();
	}
	last_err = gme_start_track(emu, (int)id);
	if (pMusicVoice != NULL)
			pMusicVoice->Start();
	if (last_err != NULL)
		return 1;
	paused = false;
	return 0;
}

GMREAL GME_Play(double count, void *out) {
	if (paused || emu == NULL || volume == 0.0) {
		memset(out, 0, (size_t)count*4);
		return 0;
	}
	last_err = gme_play(emu, (int)count*2, (short*)out);
	/* // we're doing volume differently now
	if (volume != 1.0) {
		for (int i = 0; i < count*2; i++) {
			((short*)out)[i] *= volume;
		}
	}
	*/
	if (last_err)
		return 1;
	return 0;
}

GMREAL GME_Fade(double length) {
	gme_set_fade_msecs(emu, gme_tell(emu), length * 1000);
	return 0;
}

GMREAL GME_Pause(double will_pause) {
	paused = (int)will_pause;
	return 0;
}

GMREAL GME_Length(const void *data, double size, double track) {
	// max 20 seconds
	Music_Emu* temp_emu = NULL;
	if (last_err = gme_open_data(data, (long)size, &temp_emu, 44100))
		goto error;
	if (last_err = gme_start_track(temp_emu, (int)track))
		goto error;
	short out[735*2]; // increments of 1/60 seconds
	int len = 0;
	while (!gme_track_ended(temp_emu) && gme_tell(temp_emu) < 20000) {
		if (last_err = gme_play(temp_emu, sizeof(out)/2, out))
			goto error;
		// only update length if there is nonsilence
		for (int i = 0; i < sizeof(out)/2; i++) {
			if (out[i]) {
				len = gme_tell_samples(temp_emu);
				break;
			}
		}
	}
	gme_delete(temp_emu);
	return len / 88200.0;
error:
	if (temp_emu != NULL)
		gme_delete(temp_emu);
	return -1;
}

GMREAL GME_Free() {
	if (emu != NULL) {
		gme_delete(emu);
		emu = NULL;
	}
	return 0;
}

GMREAL GME_SetVolume(double new_volume) {
	return pMusicVoice->SetVolume((float)new_volume);
}

GMREAL SFX_SetVolume(double new_volume) {
	return pSFXSubmixVoice->SetVolume((float)new_volume);
}

class MusicCallback : public IXAudio2VoiceCallback {
	public: XAUDIO2_BUFFER buf_info;
	BYTE* buf;
	const int buf_size = 1024;
	MusicCallback() : buf_info({0}) {
		buf = (BYTE*)malloc(buf_size);
		buf_info.AudioBytes = buf_size;
		buf_info.pAudioData = buf;
		buf_info.LoopBegin = XAUDIO2_NO_LOOP_REGION;
		memset(buf, 0, buf_size);
	}
	~MusicCallback() {
		free(buf);
	}
	void STDMETHODCALLTYPE OnBufferEnd(void* pBufferContext) {
		playing = true;
		GME_Play(buf_size/4, buf);
		playing = false;
		pMusicVoice->SubmitSourceBuffer(&buf_info);
	}
	
	void STDMETHODCALLTYPE OnBufferStart(void* pBufferContext) {}
	void STDMETHODCALLTYPE OnLoopEnd(void* pBufferContext) {}
	void STDMETHODCALLTYPE OnStreamEnd() {}
	void STDMETHODCALLTYPE OnVoiceError(void* pBufferContext, HRESULT Error) {
		printf("Music voice error code %#08x\n", Error);
	}
	void STDMETHODCALLTYPE OnVoiceProcessingPassEnd() {}
	void STDMETHODCALLTYPE OnVoiceProcessingPassStart(UINT32 SamplesRequired) {}

};

HMODULE g_XAudioDLL = NULL;

GMREAL sound_init() {
	HRESULT err;
	WAVEFORMATEX format;
	
	if (err = XAudio2Create(&pXAudio2)) {
		puts("XAudio2Create failed");
		return err;
	}
	if (err = pXAudio2->CreateMasteringVoice(&pMasteringVoice, 2, 44100)) {
		puts("CreateMasteringVoice failed");
		return err;
	}
	puts("mastering voice created");

	// format
	format.wFormatTag = WAVE_FORMAT_PCM;
	format.nChannels = 2;
	format.nSamplesPerSec = 44100;
	format.wBitsPerSample = 16;
	format.nBlockAlign = format.nChannels * format.wBitsPerSample / 8;
	format.nAvgBytesPerSec = format.nSamplesPerSec * format.nBlockAlign;
	format.cbSize = 0;

	// music
	pMusicCallback = new MusicCallback();
	puts("callback created");
	if (err = pXAudio2->CreateSourceVoice(&pMusicVoice, &format, 0, XAUDIO2_DEFAULT_FREQ_RATIO, pMusicCallback)) {
		puts("CreateSourceVoice (music) failed");
		return err;
	}
	if (err = pMusicVoice->SubmitSourceBuffer(&pMusicCallback->buf_info)) {
		puts("SubmitSourceBuffer failed");
		return err;
	}
	
	// sfx
	format.nChannels = 1; // mono because we can
	format.nBlockAlign = format.nChannels * format.wBitsPerSample / 8;
	format.nAvgBytesPerSec = format.nSamplesPerSec * format.nBlockAlign;
	if (err = pXAudio2->CreateSubmixVoice(&pSFXSubmixVoice, 1, 44100)) {
		puts("CreateSubmixVoice failed");
		return err;
	}
	XAUDIO2_SEND_DESCRIPTOR sendDescriptor = {0, pSFXSubmixVoice};
	XAUDIO2_VOICE_SENDS voiceSends = {1, &sendDescriptor};
	XAUDIO2_VOICE_SENDS sendList[2] = {voiceSends, NULL};
	for (int i = 0; i < SFX_CHANNEL_COUNT; i++) {
		if (err = pXAudio2->CreateSourceVoice(&(pSfxVoices[i]), &format, 0, XAUDIO2_DEFAULT_FREQ_RATIO, NULL, sendList)) {
			printf("CreateSourceVoice (sfx %i) failed\n", i);
			return err;
		}
		if (err = pSfxVoices[i]->Start(0)) {
			printf("Start (sfx %i) failed\n", i);
			return err;
		}
		last_sfx_played[i] = -1;
	}
	// sfx loop
	for (int i = 0; i < SFX_LOOP_CHANNEL_COUNT; i++) {
		if (err = pXAudio2->CreateSourceVoice(&(pSfxVoicesLoop[i]), &format, 0, XAUDIO2_DEFAULT_FREQ_RATIO, NULL, sendList)) {
			printf("CreateSourceVoice (sfx loop %i) failed\n", i);
			return err;
		}
		if (err = pSfxVoicesLoop[i]->Start(0)) {
			printf("Start (sfx loop %i) failed\n", i);
			return err;
		}
	}
	return 0;
}

GMREAL sound_load(const BYTE* data, double offset, double size) {
	if (size < 5*4) return 1;
	data += (size_t)offset;
	unsigned int* read_head = (unsigned int*)data;
	unsigned int total_sfx_count = 0;
	for (int i = 0; i < 5; i++) {
		total_sfx_count += *(read_head++);
		sfx_counts[i] = total_sfx_count;
	}
	printf("total sfx count: %i\n", total_sfx_count);
	if (size < 5*4 + total_sfx_count*4) goto invalid;
	sfx_pointers = (unsigned int*)malloc((total_sfx_count + 1) * 4);
	for (unsigned int i = 0; i <= total_sfx_count; i++) {
		sfx_pointers[i] = *(read_head++);
		if (i > 0 && sfx_pointers[i] < sfx_pointers[i-1]) // they must be in ascending order
			goto invalid;
	}
	printf("sfx size: %i\n", sfx_pointers[total_sfx_count]);
	if (size < 5*4 + total_sfx_count*4 + sfx_pointers[total_sfx_count]) goto invalid;
	sfx_data = (BYTE*)malloc(sfx_pointers[total_sfx_count]);
	memcpy(sfx_data, read_head, sfx_pointers[total_sfx_count]);
	return 0;
invalid:
	if (sfx_pointers != NULL) {
		free(sfx_pointers);
		sfx_pointers = NULL;
	}
	if (sfx_data != NULL) {
		free(sfx_data);
		sfx_data = NULL;
	}
	return 1;
}

inline void stop_voice(IXAudio2SourceVoice* voice) {
	voice->Stop();
	voice->FlushSourceBuffers();
}

struct sfx_info {
	int size;
	BYTE* data;
	int priority;
};

sfx_info sfx_get_info(int id_) {
	if (sfx_data == NULL || id_ < 0)
		return {0};
	// it's positive so this is safe
	unsigned int id = (unsigned int)id_;
	int sfx_priority;
	for (sfx_priority = 0; sfx_priority < 5; sfx_priority++) {
		if (id < sfx_counts[sfx_priority])
			break;
	}
	if (sfx_priority >= 5)
		return {0};
	
	int size = sfx_pointers[id+1] - sfx_pointers[id];
	BYTE* data = sfx_data + sfx_pointers[id];
	return {size, data, sfx_priority};
}

GMREAL sfx_length(double d_id) {
	int id = (int)d_id;
	return sfx_get_info(id).size / (44100*2.0);
}

GMREAL sfx_is_playing(double d_id) {
	int id = (int)d_id;
	sfx_info info = sfx_get_info(id);
	if (info.data == NULL)
		return 0;
	XAUDIO2_VOICE_STATE state;
	if (info.priority == 4) {
		for (int i = 0; i < SFX_LOOP_CHANNEL_COUNT; i++) {
			pSfxVoicesLoop[i]->GetState(&state);
			if (state.pCurrentBufferContext == info.data)
				return 1;
		}
		return 0;
	} else {
		for (int i = 0; i < SFX_CHANNEL_COUNT; i++) {
			pSfxVoices[i]->GetState(&state);
			if (state.pCurrentBufferContext == info.data)
				return 1;
		}
		return 0;
	}
}

GMREAL sfx_play(double d_id, double loops, double to_override) {
	HRESULT err;
	int id = (int)d_id;
	sfx_info info = sfx_get_info(id);
	if (info.data == NULL)
		return 1;

	// priority system
	IXAudio2SourceVoice* destVoice = NULL;
	if (info.priority == 4) {
		XAUDIO2_VOICE_STATE states[SFX_LOOP_CHANNEL_COUNT];
		for (int i = 0; i < SFX_LOOP_CHANNEL_COUNT; i++) {
			pSfxVoicesLoop[i]->GetState(&(states[i]));
			if (states[i].BuffersQueued == 0 || states[i].pCurrentBufferContext == info.data) {
				destVoice = pSfxVoicesLoop[i];
				last_loop_channel = i;
				break;
			}
		}
		if (destVoice == NULL) {
			last_loop_channel = !last_loop_channel;
			destVoice = pSfxVoicesLoop[last_loop_channel];
		}
	} else {
		XAUDIO2_VOICE_STATE states[SFX_CHANNEL_COUNT];
		for (int i = 0; i < SFX_CHANNEL_COUNT; i++) {
			pSfxVoices[i]->GetState(&(states[i]));
			if (states[i].BuffersQueued == 0 || last_sfx_played[i] == id || last_sfx_played[i] < 0) {
				destVoice = pSfxVoices[i];
				last_sfx_played[i] = id;
				break;
			}
		}
		if (destVoice == NULL) {
			int least_priority = 8;
			int least_priority_channel = -1;
			for (int i = 0; i < SFX_CHANNEL_COUNT; i++) {
				int sfx_priority;
				for (sfx_priority = 0; sfx_priority < 4; sfx_priority++) {
					if (last_sfx_played[i] < sfx_counts[sfx_priority])
						break;
				}
				if (sfx_priority < least_priority) {
					least_priority = sfx_priority;
					least_priority_channel = i;
				}
			}
			if (least_priority <= info.priority) {
				destVoice = pSfxVoices[least_priority_channel];
				last_sfx_played[least_priority_channel] = id;
			}
		}
	}
	if (destVoice == NULL)
		return 0;

	// actually play the sound
	XAUDIO2_BUFFER buf_info = {0};
	buf_info.Flags = XAUDIO2_END_OF_STREAM;
	buf_info.AudioBytes = info.size;
	buf_info.pAudioData = info.data;
	buf_info.pContext = (void*)(info.data);
	if (loops)
		buf_info.LoopCount = XAUDIO2_LOOP_INFINITE;
	stop_voice(destVoice);
	if (err = destVoice->SubmitSourceBuffer(&buf_info))
		printf("SubmitSourceBuffer error %#08x\n", err);
	destVoice->Start();
	return err;
}

GMREAL sfx_stop(double d_id) {
	int id = (int)d_id;
	sfx_info info = sfx_get_info(id);
	if (info.data == NULL)
		return 0;
	XAUDIO2_VOICE_STATE state;
	if (info.priority < 4) {
		for (int i = 0; i < SFX_CHANNEL_COUNT; i++) {
			pSfxVoices[i]->GetState(&state);
			if (state.pCurrentBufferContext == info.data) {
				stop_voice(pSfxVoices[i]);
				last_sfx_played[i] = -1;
			}
		}
	} else {
		for (int i = 0; i < SFX_LOOP_CHANNEL_COUNT; i++) {
			pSfxVoicesLoop[i]->GetState(&state);
			if (state.pCurrentBufferContext == info.data) {
				stop_voice(pSfxVoicesLoop[i]);
			}
		}
	}
	return 0;
}

GMREAL sfx_stop_all(double looping_only) {
	if (sfx_data == NULL) return 0;
	for (int i = 0; i < SFX_LOOP_CHANNEL_COUNT; i++) {
		stop_voice(pSfxVoicesLoop[i]);
	}
	if (!looping_only) {
		for (int i = 0; i < SFX_CHANNEL_COUNT; i++) {
			stop_voice(pSfxVoices[i]);
			last_sfx_played[i] = -1;
		}
	}
	return 0;
}

GMREAL sound_quit() {
	pXAudio2->Release();
	if (g_XAudioDLL)
		FreeLibrary(g_XAudioDLL);
	return 0;
}


/*
const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);
const IID IID_IAudioClient = __uuidof(IAudioClient);
const IID IID_IAudioRenderClient = __uuidof(IAudioRenderClient);

IMMDeviceEnumerator *deviceEnumerator = NULL;

struct sound_device {
	IAudioClient *pAudioClient;
	IAudioRenderClient *pRenderClient;
	double buf_time;
	UINT32 bufferFrameCount;
};

sound_device* sound_new_device();

DWORD WINAPI sound_loop(void* userdat_) {
	DWORD err;
	IMMDeviceEnumerator *pEnumerator = NULL;
	IMMDevice *pDevice = NULL;
	IAudioClient *pAudioClient;
	IAudioRenderClient *pRenderClient;
	WAVEFORMATEX wfx;
	WAVEFORMATEX *pwfx = &wfx;
	UINT32 bufferFrameCount;
	UINT32 numFramesAvailable = 0;
	UINT32 numFramesPadding = 0;
	BYTE *pData;
	double buf_time;
	
	sound_device device = *(sound_device*)userdat_;
	delete userdat_;
	
	
	LARGE_INTEGER last_time;
	LARGE_INTEGER new_time;
	LARGE_INTEGER timer_frequency;
	QueryPerformanceFrequency(&timer_frequency);
	QueryPerformanceCounter(&new_time);
	last_time = new_time;
	
	#define ERR(x) if (x) goto new_audio_device;
	
	while (1) {
		Sleep(device.buf_time * numFramesPadding);
		
		playing = true;
		
		ERR(device.pAudioClient->GetCurrentPadding(&numFramesPadding));
		
		numFramesAvailable = device.bufferFrameCount - numFramesPadding;
		
		
		ERR(device.pRenderClient->GetBuffer(numFramesAvailable, &pData));
		
		if (numFramesAvailable != 0) {
			GME_Play(numFramesAvailable, pData);
		}
		
		playing = false;
		
		ERR(device.pRenderClient->ReleaseBuffer(numFramesAvailable, 0));
		
		if (numFramesPadding == 0)
			printf("Buffer underrun, delay of %fms\n", (double)(new_time.QuadPart - last_time.QuadPart) * 1000 / timer_frequency.QuadPart);
		last_time = new_time;
		QueryPerformanceCounter(&new_time);

		continue;
new_audio_device:
		puts("Audio device broken, getting a new one");
		sound_device *new_device = sound_new_device();
		device = *new_device;
		delete new_device;
	}
	
	#undef ERR
	
	return 0;
}

sound_device* sound_new_device() {
	HRESULT err;
	IMMDevice *pDevice = NULL;
	IAudioClient *pAudioClient = NULL;
	IAudioRenderClient *pRenderClient = NULL;
	WAVEFORMATEX wfx;
	WAVEFORMATEX *pwfx = &wfx;
	UINT32 bufferFrameCount;
	UINT32 numFramesAvailable;
	UINT32 numFramesPadding;
	BYTE *pData;
	double buf_time;
	
	err = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &pDevice);
	printf("%d\n", err);
	
	err = pDevice->Activate(IID_IAudioClient, CLSCTX_ALL, NULL, (void**)&pAudioClient);
	printf("%d\n", err);
	
	err = pAudioClient->GetMixFormat(&pwfx);
	printf("%d\n", err);
	
	pwfx->wFormatTag = WAVE_FORMAT_PCM;
	pwfx->wBitsPerSample = 16;
	pwfx->nChannels = 2;
	sample_rate = pwfx->nSamplesPerSec;
	if (emu != NULL)
		emu->set_sample_rate(sample_rate);
	//pwfx->nSamplesPerSec = 44100;
	pwfx->nBlockAlign = pwfx->nChannels * pwfx->wBitsPerSample / 8;
	pwfx->nAvgBytesPerSec = pwfx->nSamplesPerSec * pwfx->nBlockAlign;
	pwfx->cbSize = 0;
	
	err = pAudioClient->Initialize(
		AUDCLNT_SHAREMODE_SHARED,
		0 & AUDCLNT_STREAMFLAGS_EVENTCALLBACK,
		166668*4, 0, pwfx, NULL);
	printf("%d\n", err);
	
	err = pAudioClient->GetBufferSize(&bufferFrameCount);
	printf("%d\n", err);
	
	err = pAudioClient->GetService(IID_IAudioRenderClient, (void**)&pRenderClient);
	printf("%d\n", err);
	
	err = pRenderClient->GetBuffer(bufferFrameCount, &pData);
	printf("%d\n", err);
	
	GME_Play(bufferFrameCount, pData);
	
	err = pRenderClient->ReleaseBuffer(bufferFrameCount, 0);
	printf("%d\n", err);
	
	buf_time = (double)bufferFrameCount / pwfx->nSamplesPerSec;
	
	buf_time = buf_time * 500.0 / (double)bufferFrameCount;
	
	puts("starting");
	err = pAudioClient->Start();
	printf("%d\n", err);
	
	sound_device *userdat = new sound_device();
	userdat->pAudioClient = pAudioClient;
	userdat->pRenderClient = pRenderClient;
	userdat->buf_time = buf_time;
	userdat->bufferFrameCount = bufferFrameCount;
	return userdat;
}

GMREAL sound_init() {
	
	if (sound_thread != NULL) {
		last_err = "Already initialized";
		return 1;
	}
	
	//timeBeginPeriod(5);
	
	HRESULT err;
	
	err = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	
	puts("getting enumerator");
	err = CoCreateInstance(CLSID_MMDeviceEnumerator, NULL, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&deviceEnumerator);
	printf("%d\n", err);
	
	sound_device *userdat = sound_new_device();
	
	sound_thread = CreateThread(NULL, 0, &sound_loop, userdat, 0, NULL);
	
	return 0;
}
*/