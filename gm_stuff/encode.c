#include "common.h"
#include <strsafe.h>

HANDLE vid_pipe;

GMREAL encode_begin(const char* ffmpeg_path, const char* fn) {
	// set up data structures to pass to CreateProcess
	STARTUPINFOA start_info;
	ZeroMemory(&start_info, sizeof(STARTUPINFOA));
	start_info.cb = sizeof(STARTUPINFOA);
	PROCESS_INFORMATION proc_info;
	ZeroMemory(&proc_info, sizeof(PROCESS_INFORMATION));
	SECURITY_ATTRIBUTES securatt; // no need to zero this one
	securatt.nLength = sizeof(SECURITY_ATTRIBUTES);
	securatt.lpSecurityDescriptor = NULL;
	securatt.bInheritHandle = TRUE;
	
	// make the input pipe
	HANDLE stdin_read;
	if (!CreatePipe(&stdin_read, &vid_pipe, &securatt, 100)) {
		DWORD error = GetLastError();
		printf_s("CreatePipe(stdin) failed: %d\n", error);
		return error;
	}
	
	// make the write handle un-inheritable
	if (!SetHandleInformation(vid_pipe, HANDLE_FLAG_INHERIT, 0)) {
		DWORD error = GetLastError();
		printf_s("SetHandleInformation(stdin) failed: %d\n", error);
		return error;
	}
	
	// add handles to startup info
	start_info.hStdInput = stdin_read;
	start_info.dwFlags |= STARTF_USESTDHANDLES;
	
	// create the command
	size_t cmd_size = (strlen(ffmpeg_path) + strlen(fn) + 100) * sizeof(CHAR);
	LPSTR cmd = (LPSTR)malloc(cmd_size);
	HRESULT res = StringCbPrintfA(cmd, cmd_size,
		"\"%s\" -f rawvideo -pix_fmt bgra -video_size 256x240 -framerate 60 -i pipe:0 -y \"%s\"",ffmpeg_path, fn);
	if (!SUCCEEDED(res)) {
		printf_s("StringCbPrintfW() failed: %d\n", res);
		return res;
	}
	
	// run the command
	if (0 == CreateProcessA(NULL, cmd, NULL, NULL, TRUE, CREATE_NEW_CONSOLE, NULL, NULL,
			&start_info, &proc_info)) {
		DWORD error = GetLastError();
		printf_s("CreateProcess() failed: %d\n", error);
		return error;
	}
	
	// close the handles created by CreateProcess and the pipe handle we don't need
	if (0 == CloseHandle(proc_info.hProcess)
			|| 0 == CloseHandle(proc_info.hThread)
			|| 0 == CloseHandle(stdin_read)) {
		DWORD error = GetLastError();
		printf_s("CloseHandle() failed: %d\n", error);
		return error;
	}
	
	return 0;
}

GMREAL encode_frame(const void* data) {
	DWORD bytes_written;
	if (!WriteFile(vid_pipe, data, 256*240*4, &bytes_written, NULL)) {
		DWORD error = GetLastError();
		if (ERROR_IO_PENDING != error)
			return error;
	}
	return 0;
}

GMREAL encode_end() {
	if (!CloseHandle(vid_pipe)) {
		DWORD error = GetLastError();
		if (ERROR_IO_PENDING != error)
			return error;
	}
	return 0;
}