#include "common.h"
#include <array>
#include <strsafe.h>
#include <shlwapi.h>
#include <time.h>

// to build:
// cl replay.c /link /dll /out:replay.dll
// to include the scheduler:
// ml /coff /c scheduler.asm && cl replay.c /link scheduler.obj /dll /def:scheduler.def /out:replay.dll

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "comdlg32.lib")
#pragma comment(lib, "shlwapi.lib")

LPWSTR my_path; // path to the dll this is running from
HANDLE pipe_write;
HANDLE pipe_read;
DWORD child_proc_id;

BOOL WINAPI DllMain(HINSTANCE hnd, DWORD reason, LPVOID reserved)
{
	// remove argument warnings
	(void)reserved;
	if (reason == DLL_PROCESS_ATTACH)
	{
		// get path to DLL
		int size = 0;
		int res;
		do
		{
			// if out of memory, try again
			if (size == 0 || my_path != NULL)
				size += 100;
			else
				size = 100;
			my_path = (LPWSTR)realloc(my_path, size * sizeof(WCHAR));
			// GetModuleFileNameW returns string size including \0 on
			// insufficient buffer, and string size not including \0 on success
		} while (NULL == my_path || (res = GetModuleFileNameW(hnd, my_path, size)) >= size);
		if (res == 0) // GetModuleFileNameW failed
			return FALSE;
	}
	else if (reason == DLL_PROCESS_DETACH)
	{
		free(my_path);
		my_path = NULL;
	}
	return TRUE;
}

GMREAL dll_loaded()
{ // if GM can't find a DLL, all functions return zero
	return 1;
}

GMREAL send_u8(double i)
{
	DWORD bytes_written;
	unsigned char conv = (unsigned char)i;
	if (!WriteFile(pipe_write, &conv, 1, &bytes_written, NULL))
	{
		DWORD error = GetLastError();
		if (ERROR_IO_PENDING != error)
			return error;
	}
	return 0;
}

GMREAL send_u16(double i)
{
	DWORD bytes_written;
	unsigned short conv = (unsigned short)i;
	if (!WriteFile(pipe_write, &conv, 2, &bytes_written, NULL))
	{
		DWORD error = GetLastError();
		if (ERROR_IO_PENDING != error)
			return error;
	}
	return 0;
}

GMREAL send_string(const char *s)
{
	DWORD bytes_written;
	DWORD len = (DWORD)strlen(s);
	if (len == 0) // they probably want to open a file dialog so let it take focus
		AllowSetForegroundWindow(child_proc_id);
	DWORD error = (DWORD)send_u16(len);
	if (error != 0)
		return error;
	if (!WriteFile(pipe_write, s, len, &bytes_written, NULL))
	{
		DWORD error = GetLastError();
		if (ERROR_IO_PENDING != error)
			return error;
	}
	return 0;
}

GMREAL recv_u8(double i)
{
	// returns -1 if there is no data, -2 on error
	unsigned char res;
	DWORD bytes_read, bytes_avail, bytes_left;
	if (!PeekNamedPipe(pipe_read, &res, 1, &bytes_read, &bytes_avail, &bytes_left))
	{
		printf_s("Error code: %d\n", GetLastError());
		return -2;
	}
	if (bytes_read <= 0)
		return -1;
	if (!ReadFile(pipe_read, &res, 1, &bytes_read, NULL))
	{
		return -2;
	}
	return res;
}

GMREAL recv_u32(double i)
{
	// returns -1 if there is no data, -2 on error
	unsigned int res;
	DWORD bytes_read, bytes_avail, bytes_left;
	if (!PeekNamedPipe(pipe_read, &res, 4, &bytes_read, &bytes_avail, &bytes_left))
	{
		printf_s("Error code: %d\n", GetLastError());
		return -2;
	}
	if (bytes_read < 4)
		return -1;
	if (!ReadFile(pipe_read, &res, 4, &bytes_read, NULL))
	{
		return -2;
	}
	return res;
}

GMREAL recv_data(void *buffer, double len)
{
	// returns -1 if there is no data, -2 on error
	DWORD bytes_read, bytes_avail, bytes_left;
	if (!PeekNamedPipe(pipe_read, buffer, (DWORD)len, &bytes_read, &bytes_avail, &bytes_left))
	{
		printf_s("Error code: %d\n", GetLastError());
		return -2;
	}
	if (bytes_read < len)
		return -1;
	if (!ReadFile(pipe_read, buffer, (DWORD)len, &bytes_read, NULL))
	{
		return -2;
	}
	return 0;
}

GMREAL setup_replay_helper(const char *version_number)
{
	// set up data structures to pass to CreateProcess
	STARTUPINFOW start_info;
	ZeroMemory(&start_info, sizeof(STARTUPINFOW));
	start_info.cb = sizeof(STARTUPINFOW);
	PROCESS_INFORMATION proc_info;
	ZeroMemory(&proc_info, sizeof(PROCESS_INFORMATION));
	SECURITY_ATTRIBUTES securatt; // no need to zero this one
	securatt.nLength = sizeof(SECURITY_ATTRIBUTES);
	securatt.lpSecurityDescriptor = NULL;
	securatt.bInheritHandle = TRUE;

	// make the input pipe
	HANDLE stdin_read;
	if (!CreatePipe(&stdin_read, &pipe_write, &securatt, 100))
	{
		DWORD error = GetLastError();
		printf_s("CreatePipe(stdin) failed: %d\n", error);
		return error;
	}

	// make the write handle un-inheritable
	if (!SetHandleInformation(pipe_write, HANDLE_FLAG_INHERIT, 0))
	{
		DWORD error = GetLastError();
		printf_s("SetHandleInformation(stdin) failed: %d\n", error);
		return error;
	}

	// make the output pipe
	HANDLE stdout_write;
	if (!CreatePipe(&pipe_read, &stdout_write, &securatt, 100))
	{
		DWORD error = GetLastError();
		printf_s("CreatePipe(stdout) failed: %d\n", error);
		return error;
	}
	// make the read handle un-inheritable
	if (!SetHandleInformation(pipe_read, HANDLE_FLAG_INHERIT, 0))
	{
		DWORD error = GetLastError();
		printf_s("SetHandleInformation(stdout) failed: %d\n", error);
		return error;
	}

	// add handles to startup info
	start_info.hStdInput = stdin_read;
	start_info.hStdOutput = stdout_write;
	start_info.hStdError = stdout_write;
	start_info.dwFlags |= STARTF_USESTDHANDLES;

	// create the command
	size_t cmd_size = (wcslen(my_path) + 40) * sizeof(WCHAR);
	LPWSTR cmd = (LPWSTR)malloc(cmd_size);
	if (cmd == NULL)
	{
		printf_s("malloc(cmd_size) failed: out of memory");
		return ERROR_NOT_ENOUGH_MEMORY;
	}
	HRESULT res = StringCbPrintfW(cmd, cmd_size,
								  L"rundll32 \"%s\",RealMain", my_path);
	if (!SUCCEEDED(res))
	{
		printf_s("StringCbPrintfW() failed: %d\n", res);
		return res;
	}

	// run the command
	if (0 == CreateProcessW(NULL, cmd, NULL, NULL, TRUE, 0, NULL, NULL,
							&start_info, &proc_info))
	{
		DWORD error = GetLastError();
		printf_s("CreateProcess() failed: %d\n", error);
		return error;
	}

	// close the handles created by CreateProcess and the pipe handle we don't need
	if (0 == CloseHandle(proc_info.hProcess) || 0 == CloseHandle(proc_info.hThread) || 0 == CloseHandle(stdin_read) || 0 == CloseHandle(stdout_write))
	{
		DWORD error = GetLastError();
		printf_s("CloseHandle() failed: %d\n", error);
		return error;
	}

	// save the helper's PID
	child_proc_id = proc_info.dwProcessId;
	
	// send version number
	send_string(version_number);

	return 0;
}

/* *************************** USED BY RUNDLL32 *************************** */

int save_to_file(const char *fn, void *buffer, size_t length)
{
	FILE *f;
	if (fopen_s(&f, fn, "wb"))
		return 1;
	fwrite(buffer, 1, length, f);
	fclose(f);
	return 0;
}

int get_filename(char *fn, int fn_size, char *version)
{
	// initialize filename to current time
	time_t t = time(NULL);
	tm my_tm;
	localtime_s(&my_tm, &t);
	sprintf_s(fn, fn_size, "%04d-%02d-%02d %02d-%02d - BUILD %s.rpy",
		my_tm.tm_year + 1900, my_tm.tm_mon + 1, my_tm.tm_mday + 1, my_tm.tm_hour, my_tm.tm_min, version);
	// set up filename dialog
	OPENFILENAMEA openfn;
	ZeroMemory(&openfn, sizeof(OPENFILENAMEA));
	openfn.lStructSize = sizeof(OPENFILENAMEA);
	openfn.lpstrFilter = "Replay (*.rpy)\0*.rpy\0";
	openfn.lpstrFile = fn;
	openfn.nMaxFile = fn_size;
	openfn.Flags = OFN_NOREADONLYRETURN;
	openfn.lpstrDefExt = "rpy";
	return GetSaveFileNameA(&openfn);
}

typedef std::array<char, 32> version_t;

class ReplayServer
{
private:
	int replay_buffer_size = 0;
	unsigned char *replay_buffer_cursor = NULL;
	unsigned char *replay_buffer = NULL;
	short last_inputs;
	int hold_count;
	BOOL first_input = FALSE;
	HANDLE hStdin;
	HANDLE hStdout;
	version_t version_number;

public:
	ReplayServer(HANDLE hStdin, HANDLE hStdout, version_t version_number)
		: hStdin(hStdin), hStdout(hStdout), version_number(version_number) {}
	void start_replay()
	{
		DWORD bytes_read;
		// discard any existing replay and start a new one with the given header
		// discard replay
		free(replay_buffer);
		// get header length
		unsigned short head_len;
		if (!ReadFile(hStdin, &head_len, 2, &bytes_read, NULL))
			return; // if we error once we'll error again
		// allocate replay buffer
		replay_buffer_size = head_len + 100;
		replay_buffer = (unsigned char *)malloc(replay_buffer_size);
		if (NULL == replay_buffer)
		{
			// out of memory
			return;
		}
		replay_buffer[head_len] = '\0';
		// read header into buffer
		if (!ReadFile(hStdin, replay_buffer, head_len, &bytes_read, NULL))
			return; // if we error once we'll error again
		replay_buffer_cursor = replay_buffer + head_len + 1;
		first_input = TRUE;
		last_inputs = -1;
		hold_count = 0;
	}
	void push_data(void *data, int size)
	{
		// check if we need to make the buffer wider
		int replay_buffer_pos = replay_buffer_cursor - replay_buffer;
		if (replay_buffer_pos + size + 3 > replay_buffer_size)
		{
			// widen buffer
			replay_buffer_size += 100;
			unsigned char *new_buffer = (unsigned char *)realloc(replay_buffer, replay_buffer_size);
			if (NULL == new_buffer)
			{
				// out of memory, cancel replay
				free(replay_buffer);
				replay_buffer = NULL;
				return;
			}
			replay_buffer = new_buffer;
			replay_buffer_cursor = replay_buffer + replay_buffer_pos;
		}
		// actually write the data
		memcpy(replay_buffer_cursor, data, size);
		replay_buffer_cursor += size;
	}
	void delimit_buffer()
	{
		// don't if we haven't pressed anything yet
		if (-1 != last_inputs)
		{
			// write last inputs to buffer
			int new_unit = (((int)last_inputs) << 12) | hold_count;
			// we can assume little endian
			push_data(&new_unit, 3);
		}
		last_inputs = -1;
		hold_count = 0;
	}
	void accept_input()
	{
		DWORD bytes_read;
		// inputs incoming
		// get inputs
		short inputs;
		if (!ReadFile(hStdin, &inputs, 2, &bytes_read, NULL))
			return; // if we error once we'll error again
		if (NULL != replay_buffer)
		{
			if (inputs != last_inputs || hold_count + 1 >= (1 << 12))
			{
				// new inputs
				delimit_buffer();
				last_inputs = inputs;
			}
			hold_count++;
		}
	}
	void save()
	{
		DWORD bytes_read;
		unsigned char message_type;
		// save replay to file
		// get path length
		unsigned short path_len;
		if (!ReadFile(hStdin, &path_len, 2, &bytes_read, NULL))
			return; // if we error once we'll error again
		char *path_buffer = NULL;
		if (path_len > 0)
		{
			// allocate filepath buffer
			char *path_buffer = (char *)malloc(path_len + 1);
			if (NULL == path_buffer)
			{
				// out of memory
				message_type = 3;
				WriteFile(hStdout, &message_type, 1, &bytes_read, NULL);
				return;
			}
			path_buffer[path_len] = '\0';
			// read filepath into buffer
			if (!ReadFile(hStdin, path_buffer, path_len, &bytes_read, NULL))
			{
				free(path_buffer);
				// attempt to send error response
				message_type = 3; // error
				WriteFile(hStdout, &message_type, 1, &bytes_read, NULL);
				return; // if we error once we'll error again
			}
		}
		else if (NULL != replay_buffer)
		{
			// no path provided, let's pick one ourselves
			path_buffer = (char *)malloc(MAX_PATH + 1);
			if (NULL == path_buffer)
			{
				// out of memory
				message_type = 3;
				WriteFile(hStdout, &message_type, 1, &bytes_read, NULL);
				return;
			}
			if (!get_filename(path_buffer, MAX_PATH, version_number.data()))
			{
				free(path_buffer);
				// attempt to send response
				if (CommDlgExtendedError() != 0)
					message_type = 3; // error
				else
					message_type = 1; // cancelled
				WriteFile(hStdout, &message_type, 1, &bytes_read, NULL);
				return;
			}
		}
		// only write replay if it's recording
		if (NULL != replay_buffer)
		{
			// write current inputs to buffer
			int new_unit = (((int)last_inputs) << 12) | hold_count;
			replay_buffer_cursor[0] = new_unit & 255;
			replay_buffer_cursor[1] = (new_unit >> 8) & 255;
			replay_buffer_cursor[2] = (new_unit >> 16) & 255;
			// save
			int failed = save_to_file(path_buffer, replay_buffer, replay_buffer_cursor - replay_buffer + 3);
			if (!failed)
				message_type = 0; // success
			else
				message_type = 3; // error
		}
		else
		{
			message_type = 2; // no replay
		}
		WriteFile(hStdout, &message_type, 1, &bytes_read, NULL);
		// free filepath buffer
		free(path_buffer);
	}
	void end_replay()
	{
		// end replay recording
		free(replay_buffer);
		replay_buffer = NULL;
		first_input = TRUE;
	}
	void debug_command()
	{
		DWORD bytes_read;
		unsigned char cmd;
		if (!ReadFile(hStdin, &cmd, 1, &bytes_read, NULL))
			return;
		delimit_buffer();
		int command_marker = 0xFFFFFF;
		push_data(&command_marker, 3);
		push_data(&cmd, 1);
	}
	void replay_data() {
		// write current inputs to buffer
		int new_unit = (((int)last_inputs) << 12) | hold_count;
		replay_buffer_cursor[0] = new_unit & 255;
		replay_buffer_cursor[1] = (new_unit >> 8) & 255;
		replay_buffer_cursor[2] = (new_unit >> 16) & 255;
		// send out
		DWORD bytes_read;
		unsigned int size = replay_buffer_cursor - replay_buffer + 3;
		WriteFile(hStdout, &size, 4, &bytes_read, NULL);
		WriteFile(hStdout, replay_buffer, size, &bytes_read, NULL);
	}
	void error_check() {
		// the parent process was closed
		if (NULL != replay_buffer)
		{
			int res = MessageBox(NULL,
									TEXT(
										"Looks like the game closed unexpectedly.\n"
										"Do you want to save a replay of what you were doing?"),
									NULL,
									MB_ICONERROR | MB_SYSTEMMODAL | MB_YESNO);
			if (res == IDYES)
			{
				char fn[MAX_PATH + 1];
				if (get_filename(fn, MAX_PATH, version_number.data()))
				{
					// write current inputs to buffer
					int new_unit = (((int)last_inputs) << 12) | hold_count;
					replay_buffer_cursor[0] = new_unit & 255;
					replay_buffer_cursor[1] = (new_unit >> 8) & 255;
					replay_buffer_cursor[2] = (new_unit >> 16) & 255;
					// save
					save_to_file(fn, replay_buffer, replay_buffer_cursor - replay_buffer + 3);
				}
			}
		}
	}
};

void RealMain(HWND hWnd, HINSTANCE hPrevInstance, LPSTR lpCmdLine,
			  int nCmdShow)
{
// remove name decoration
#pragma comment(linker, "/EXPORT:" __FUNCTION__ "=" __FUNCDNAME__)
	// remove argument warnings
	(void)hWnd;
	(void)hPrevInstance;
	(void)lpCmdLine;
	(void)nCmdShow;

	// open input pipe
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
	DWORD error;
	if (hStdin == INVALID_HANDLE_VALUE)
	{
		TCHAR error_message[30];
		StringCchPrintf(error_message, 30, TEXT("Unknown error code %d"), GetLastError());
		MessageBox(NULL, error_message, NULL, MB_ICONERROR);
		return;
	}
	if (hStdin == NULL)
	{
		MessageBox(NULL, TEXT("hStdin is NULL"), NULL, 0);
		return;
	}

	// open output pipe
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdout == INVALID_HANDLE_VALUE)
	{
		TCHAR error_message[30];
		StringCchPrintf(error_message, 30, TEXT("Unknown error code %d"), GetLastError());
		MessageBox(NULL, error_message, NULL, MB_ICONERROR);
		return;
	}
	if (hStdout == NULL)
	{
		MessageBox(NULL, TEXT("hStdout is NULL"), NULL, 0);
		return;
	}
	
	DWORD bytes_read;
	
	// get version number
	version_t version_number;
	{
		short version_length;
		ReadFile(hStdin, &version_length, 2, &bytes_read, NULL);
		ReadFile(hStdin, version_number.data(), version_length, &bytes_read, NULL);
		version_number[version_length] = 0;
		
	}

	ReplayServer server(hStdin, hStdout, version_number);
	unsigned char message_type;

	// 0: start replay and discard any existing buffer, header (string) follows
	// 1: inputs (unsigned short) follow
	// 2: save replay, save path (string) follows, sends a u8 back
	// 3: end replay (should be sent on exit)

	// responses from save replay:
	// 0: success
	// 1: GetSaveFileName failed, probably cancelled
	// 2: replay is not recording
	// >= 3: saving failed

	while (TRUE)
	{
		if (ReadFile(hStdin, &message_type, 1, &bytes_read, NULL))
		{
			// we got mail!
			if (0 == message_type)
			{
				server.start_replay();
			}
			else if (1 == message_type)
			{
				server.accept_input();
			}
			else if (2 == message_type)
			{
				server.save();
			}
			else if (3 == message_type)
			{
				server.end_replay();
			}
			else if (4 == message_type)
			{
				server.debug_command();
			}
			else if (5 == message_type)
			{
				server.replay_data();
			}
		}
		else
		{
			// error
			error = GetLastError();
			if (ERROR_BROKEN_PIPE == error)
			{
				server.error_check();
			}
			else
			{
				// unrecognized error
				TCHAR error_message[30];
				StringCchPrintf(error_message, 30, TEXT("Unknown error code %d"), GetLastError());
				MessageBox(NULL, error_message, NULL, MB_ICONERROR);
			}
			return;
		}
	}
}